package com.jogodavelha_v2.api_jogo.model;

public class Jogo {
	private Tabuleiro tabuleiro = new Tabuleiro();
	private Jogador[] jogadores = new Jogador[2];
	private int idRodada;
	private int jogadorAtivo = 0;
	private boolean encerrado = false;
	private boolean vitoria = false;
	private Valor[] valores = {Valor.X, Valor.O};


	public void jogar(int x, int y){
		if(encerrado){
			return;
		}

		Valor valorDaVez = valores[jogadorAtivo];

		boolean sucesso = tabuleiro.setCasa(x, y, valorDaVez);

		if(!sucesso){
			return;
		}

		vitoria = tabuleiro.verificarVitoria();
		boolean velha = tabuleiro.verificarVelha();

		if(vitoria || velha){
			encerrado = true;
			return;
		}

		if(jogadorAtivo == 0){
			jogadorAtivo = 1;
		}else{
			jogadorAtivo = 0;
		}
	}

	public String[][] getCasas(){
		return tabuleiro.getCasas();
	}

	public boolean isEncerrado(){
		return encerrado;
	}

	public boolean isVitoria(){
		return vitoria;
	}

	public int getJogadorAtivo(){
		return jogadorAtivo;
	}


	public int getIdRodada() {
		return idRodada;
	}
	public void setIdRodada(int idRodada) {
		this.idRodada = idRodada;
	}
	static int idJogo = 1;

	public static int getIdJogo() {
		return idJogo;
	}
	public static void setIdJogo(int idJogo) {
		Jogo.idJogo = idJogo;
	}

	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}

	public void setTabuleiro(Tabuleiro tabuleiro) {
		this.tabuleiro = tabuleiro;
	}
	public Jogador[] getJogadores() {
		return jogadores;
	}
	public void setJogadores(Jogador[] jogadores) {
		this.jogadores = jogadores;
	}


}
