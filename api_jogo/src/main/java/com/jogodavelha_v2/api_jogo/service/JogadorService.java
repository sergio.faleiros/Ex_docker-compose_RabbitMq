package com.jogodavelha_v2.api_jogo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jogodavelha_v2.api_jogo.model.Jogador;

@Service
public class JogadorService {
	
	@Autowired
	RestTemplate restTemplate;
	
	public Jogador buscarJogador(int idJogador) {
		
		Jogador jogador = restTemplate.getForObject("http://10.162.111.162:8080/jogador/listar/"+idJogador, Jogador.class);
		
		return jogador;
		
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

}
